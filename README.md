# Run

Open com.xziit.tyy.TyyServiceApplication.java and Run from main method
Server will start will listen on http://localhost:8081/api/tyy/

# Posting Data
Open postman
Select new POST request
URL: http://localhost:8081/api/tyy/
HEADER: content-type: application/json
BODY: type > raw and post json data in text field
{
	"message":"ABSBSBS ABSBSDS",
	"entryType":"Y'QD",
	"sessionId": "123123"
}
hit send

in case of invalid request api will return HTTP 500
in case of valid request HTTP 200

# Getting Data
To display message to console
GET http://localhost:8081/api/tty?id=1
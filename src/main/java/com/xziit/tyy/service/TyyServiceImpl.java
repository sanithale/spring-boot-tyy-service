package com.xziit.tyy.service;

import com.xziit.tyy.model.TyyData;
import org.springframework.stereotype.Service;

@Service("tyyService")
public class TyyServiceImpl implements TyyService {
    @Override
    public Boolean validateData(TyyData tyyData) {
        switch (tyyData.getEntryType().trim().toUpperCase()) {
            case "Y'QD":
                String[] values = tyyData.getMessage().split(" ");
                if(values.length > 32) return false;
                for (String value : values) {
                    if (!(value.length() == 7)) return false;
                }
                if (tyyData.getMessage().length() > 69) return false;
                break;
            case "Y'.":
                if (!(tyyData.getMessage().length() == 7)) return false;
                if (tyyData.getMessage().length() > 69) return false;
                break;
            case "Y'":
                if (tyyData.getMessage().length() > 69) return false;
                // TODO : Special Character Validation
                break;
            case "YET":
                // No validation
                break;
            case "YIG":
                // No Validation
            default:
                return false; // Entry Type Did Not Matched hence invalid
        }
        return true;
    }

    @Override
    public void saveData(TyyData tyyData) {

    }
}

package com.xziit.tyy.service;

import com.xziit.tyy.model.TyyData;

public interface TyyService {

    Boolean validateData(TyyData tyyData);

    void saveData(TyyData tyyData);
}

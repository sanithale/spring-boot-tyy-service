package com.xziit.tyy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages={"com.xziit.tyy"})
public class TyyServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TyyServiceApplication.class, args);
	}
}

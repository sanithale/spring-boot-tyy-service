package com.xziit.tyy.controller;

import com.xziit.tyy.model.TyyData;
import com.xziit.tyy.service.TyyService;
import com.xziit.tyy.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping("/api")
public class TyyServiceController {

    public static final Logger logger = LoggerFactory.getLogger(TyyServiceController.class);

    @Autowired
    TyyService tyyService; //Service which will do all data retrieval/manipulation work

    @RequestMapping(value = "/tyy/", method = RequestMethod.POST)
    public ResponseEntity<?> postData(@RequestBody TyyData tyyData, UriComponentsBuilder ucBuilder) {
        logger.info("Reading Input Stream", tyyData.toString());

        if (!tyyService.validateData(tyyData)) {
            logger.error("Invalid Data");
            return new ResponseEntity(new CustomErrorType("Invalid Data"),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
        tyyService.saveData(tyyData);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/api/user/{id}").buildAndExpand(tyyData.getSessionId()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.OK);
    }

}
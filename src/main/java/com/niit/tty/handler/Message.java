package com.niit.tty.handler;

import com.niit.tty.model.TtyData;
import com.xziit.tyy.model.TyyData;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by abc on 11-01-2019.
 */
public class Message {
    private String sessionId;
    private int id;
    private int intQDCount = 0;
    private int intQRCount = 0;
    private boolean receiverAdd = false;
    private boolean completeMessage = false;
    private String entries;
    //private Map <String, String> entries = new HashMap<>();

    public Message(String sessionId) {
        this.sessionId = sessionId;
    }

    public void incrementQDCount() { intQDCount++; }
    public void incrementQRCount() { intQRCount++; }
    public boolean maxQDCount() { return !(intQDCount > 32); }
    public boolean minQDCount() { return intQDCount > 0; }
    public boolean maxQRCount() { return !(intQRCount > 1); }
    public void reciverAdd() { receiverAdd = true; }
    public void completeMessage() { completeMessage = true; }

    public boolean isReceiverAdd() {
        return receiverAdd;
    }

    public boolean isCompleteMessage() {
        return completeMessage;
    }

    public String getEntries() {
        return entries;
    }

    public void saveEntry(TtyData ttyData) {
        if(entries == null)
            entries = "";
        this.entries = this.entries + "{'sessionId': "+sessionId+", 'entryType': "+ttyData.getEntryType()+", 'message': "+ttyData.getMessage()+"}\n";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {

        this.id = id;
    }
}

package com.niit.tty.messageSwitching;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages={"com.niit.tty"})
public class MessageSwitchingApplication {

	public static void main(String[] args) {
		SpringApplication.run(MessageSwitchingApplication.class, args);

	}
}
package com.niit.tty.service;

import java.util.*;

import com.niit.tty.handler.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.niit.tty.model.TtyData;
import com.niit.tty.util.ValidationErrorMessage;


@Service("ttyService")
public class TyyServiceImpl implements TtyService {

	public static final Logger logger = LoggerFactory.getLogger(TyyServiceImpl.class);
	Map<String, Message> messages = new HashMap<>();
	Map<Integer, Message> previousMessages = new HashMap();
	public int messageCounter = 1;

	@Override
	public ValidationErrorMessage validateData(TtyData ttyData) {
		Message message = messages.get(ttyData.getSessionId());
		if (message == null) {
			message = new Message(ttyData.getSessionId());
			messages.put(ttyData.getSessionId(),message);
		}
		boolean result = false;
		switch (ttyData.getEntryType().trim().toUpperCase()) {
		case "Y'QD":
			result = message.minQDCount();
			if (result)
				return new ValidationErrorMessage("INVALID ADDRESS FIELD");
			String[] values = ttyData.getMessage().split(" ");
			if (values.length <= 0)
				return new ValidationErrorMessage("INVALID DATA ENTERED CHECK INPUT");
			for (String value : values) {
				if (!(value.length() == 7))
					return new ValidationErrorMessage("INVALID DATA ENTERED CHECK INPUT");
				message.incrementQDCount();
			}
			result = message.maxQDCount();
			if (!result)
				return new ValidationErrorMessage("INVALID DATA ENTERED CHECK INPUT");
			break;
		case "Y'.":
			result = message.minQDCount();
			if (!result)
				return new ValidationErrorMessage("INVALID DATA ENTERED CHECK INPUT");
			String[] valuess = ttyData.getMessage().split(" ");
			if (valuess.length <= 0)
				return new ValidationErrorMessage("ONLY ONE SIGNATURE ALLOWED");
			for (String value : valuess) {
				if (!(value.length() == 7))
					return new ValidationErrorMessage("INVALID DATA ENTERED CHECK INPUT");
				message.incrementQRCount();
			}
			result = message.maxQRCount();
			if (!result)
				return new ValidationErrorMessage("ONLY ONE SIGNATURE ALLOWED");
			message.reciverAdd();
			break;
			
		case "Y'":
			if (!message.isReceiverAdd())
				return new ValidationErrorMessage("INVALID DATA ENTERED CHECK INPUT ");
			if (ttyData.getMessage().length() <= 0)
				return new ValidationErrorMessage("#FORMAT#");
			message.completeMessage();
			// TODO : Special Character Validation
			break;
		case "YET":
			result = message.minQDCount();
			if (!result)
				return new ValidationErrorMessage("NO MSGS");
			if (!message.isCompleteMessage())
				return new ValidationErrorMessage("Message Body Not Found");
			break;
		case "YIG":
			// No Validation
			break;
		default:
			return new ValidationErrorMessage("NO ENTRY TYPE/CRITERIA MATCHED"); // Entry Type Did Not Matched hence invalid
		}
		return new ValidationErrorMessage("NO ERROR", false);
	}
	
	@Override
	public void saveData(TtyData ttyData) {
		if (ttyData.getEntryType().equalsIgnoreCase("YIG")) // clear previous entries
			messages.remove(ttyData.getSessionId());
		else if (ttyData.getEntryType().equalsIgnoreCase("YET")) { // end of entries in message
			Message message = messages.get(ttyData.getSessionId());
			message.saveEntry(ttyData);
			displayEntriesToConsole(messageCounter, message.getEntries());
			storeMessage(ttyData.getSessionId());
		} else
			messages.get(ttyData.getSessionId()).saveEntry(ttyData);
	}

	private void storeMessage(String sessionId) {
		Message message = messages.get(sessionId);
		message.setId(messageCounter);
		previousMessages.put(message.getId(), message);
		messageCounter++;
		messages.remove(sessionId); // Reset the entries for new message's entries
	}

	private String displayEntriesToConsole(int id, String entries) {
		String message = "{'id':" + id + ", message:[" + entries + "]}";
		logger.info(message);
		return message;
	}

	@Override
	public String getMessage(int id) {
		String entries = previousMessages.get(id).getEntries();
		if (entries != null)
			return displayEntriesToConsole(id, entries);
		return "NO MESSAGE FOUND WITH ID " + id;
	}

	@Override
	public String getMessageBySession(int sessionId) {
		Message message = messages.get(sessionId+"");
		if (message != null) {
			logger.info(message.getEntries());
			return message.getEntries();
		}
		logger.error("NO MESSAGE FOUND WITH ID " + sessionId);
		return "NO MESSAGE FOUND WITH ID " + sessionId;
	}
}
